extends Control

var playButton

func _ready():
	
	playButton = get_node("CenterContainer/Panel/TextureRect/Button")
	playButton.connect("pressed", self, "newGame")
	#pause has to come after the button is created
	get_tree().set_pause(true)
	
func newGame():
	get_tree().set_pause(false)
	#resets the whole game before beginning and after win
	GameManager.resetGame()
	#deletes the scene that you are refering to (in this case self)
	queue_free()

func win():
	#change graphics from title to complete
	$CenterContainer/Panel/TextureRect/.set_texture(load("res://assets/background/tela_final.png"))
	#change line of text
	$CenterContainer/Panel/TextureRect/Label.text = "Você encontrou "+ str(GameManager.deck.size()/2) +" pares em "+ str(GameManager.seconds) +" segundos, e virou "+ str(GameManager.moves) +" pares de cartas!"
	$CenterContainer/Panel/TextureRect/Button.text = "JOGAR NOVAMENTE"
