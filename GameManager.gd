extends Node

#onready para ter certeza de que tudo foi carregado antes da chamada
onready var Game = get_node('/root/Game/')

var deck = Array()
var cardBack = preload("res://assets/cards/back.png")
var card1
var card2
var matchTimer = Timer.new()
var flipTimer = Timer.new()
var secondsTimer = Timer.new()
var score = 0
var seconds = 0
var moves = 0
var scoreLabel
var timerLabel
var movesLabel
var resetButton
var popUp = preload("res://PopUp.tscn")
var goal #change for the number of pairs you have (can only be set to deck.size() after dealing cards, see dealDeck function)
var audioPlayer = AudioStreamPlayer.new()
#file storing bird names
onready var file = "res://assets/molas.txt"
var line = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	setupHUD()
	fillDeck()
	dealDeck()
	setupTimers()
	var splash = popUp.instance()
	Game.add_child(splash)
	add_child(audioPlayer)
	#changing pause mode from code so the sound of the last bird keeps playing after the game is finished 
	#and the player can hear that last bird
	audioPlayer.pause_mode = PAUSE_MODE_INHERIT
	Game.get_node("molaNames").text = ""
	#print(goal)
	
func resetGame():
	for c in range(deck.size()):
		deck[c].queue_free()
	deck.clear()
	score = 0
	seconds = 0
	moves = 0
	scoreLabel.text = str(score)
	timerLabel.text = str(seconds)
	movesLabel.text = str(moves)
	Game.get_node("molaNames").text = ""
	fillDeck()
	dealDeck()
	audioPlayer.stop()
	secondsTimer.start()

func setupHUD():
	scoreLabel = Game.get_node("HUD/Panel/Sections/SectionScore/score")
	timerLabel = Game.get_node("HUD/Panel/Sections/SectionTimer/seconds")
	movesLabel = Game.get_node("HUD/Panel/Sections/SectionMoves/moves")
	scoreLabel.text = str(score)
	timerLabel.text = str(seconds)
	movesLabel.text = str(moves)
	resetButton = Game.get_node("HUD/Panel/Sections/SectionButtons/ButtonReset")
	resetButton.connect("pressed", self, "resetGame")
	
func setupTimers():
	flipTimer.connect("timeout", self, "turnOverCards")
	#for the timer to run only once
	flipTimer.set_one_shot(true)
	add_child(flipTimer)
	
	matchTimer.connect("timeout", self, "matchCardsAndScore")
	matchTimer.set_one_shot(true)
	add_child(matchTimer)
	
	secondsTimer.connect("timeout", self, "countSeconds")
	add_child(secondsTimer)
	secondsTimer.start()
	
func countSeconds():
	seconds += 1
	timerLabel.text = str(seconds)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func fillDeck():
	for i in range(2):
		for c in range(9):
			deck.append(Card.new(c+1))
		
func dealDeck():
	#Game.get_node('grid').add_child(deck[0])
	randomize() #gets new seed every time
	deck.shuffle()
	for c in range(deck.size()):
		Game.get_node('grid').add_child(deck[c])
		
	goal = deck.size()/2 #can only set this variable after dealing cards
	
func chooseCard(var c):
	if card1 == null:
		card1 = c
		card1.flip()
		card1.set_disabled(true)
	elif card2 == null:
		card2 = c
		card2.flip()
		card2.set_disabled(true)
		moves += 1
		movesLabel.text = str(moves)
		checkCards()
	
func checkCards():
	if card1.value == card2.value:
		matchTimer.start(1)
		#how to read from text files 
		#https://godotengine.org/qa/57130/how-to-import-and-read-text
		#https://docs.godotengine.org/en/stable/classes/class_file.html#class-file-method-get-line
		#https://godotengine.org/qa/57130/how-to-import-and-read-text
		#https://godotengine.org/qa/17740/label-get-a-line
		#https://github.com/godotengine/godot/issues/1022
		var f = File.new()
		#open and prepare to read the file 
		f.open(file, File.READ)
		
		#iterate until it reaches the desired line
		for l in range (card1.value):
			line = f.get_line()
		#print(line)
		#store a line from the file
		Game.get_node("molaNames").text = line
		
		if audioPlayer.is_playing():
			audioPlayer.stop()
			audioPlayer.stream = load("res://assets/audio/audio"+str(card1.value)+".ogg")
			audioPlayer.play()
		elif !audioPlayer.is_playing():
			audioPlayer.stream = load("res://assets/audio/audio"+str(card1.value)+".ogg")
			audioPlayer.play()
	else:
		flipTimer.start(2)

func turnOverCards():
	card1.flip()
	card2.flip()
	card1.set_disabled(false)
	card2.set_disabled(false)
	card1 = null
	card2 = null

func matchCardsAndScore():
	score +=1
	scoreLabel.text = str(score)
	card1.set_modulate(Color(0.6,0.6,0.6,1))
	card2.set_modulate(Color(0.6,0.6,0.6,1))
	card1 = null
	card2 = null
	if score == goal:
		#2 seconds before the win screen shows
		secondsTimer.stop()
		#would be nicer to detect when sound stops playing to then show the win screen
		yield(get_tree().create_timer(7.0), "timeout")
		var winScreen = popUp.instance()
		Game.add_child(winScreen)
		winScreen.win()
