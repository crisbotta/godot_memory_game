# Godot Memory Game

Memory game created in Godot 3.4 in partnership with the educators of the "Amerindian Encounters" exhibition at SESC VIla Mariana. By playing the game people get to know a type of embroidery, called Mola, made by artists of the Guna people. Play with the sound on to hear the name of the work and the artist who produced it.

This game was born from the will to create a digital interactive presence for the exhibition "Amerindian Encounters", and make it possible for a larger number of people to access the exhibition's content.

https://sescvilamariana.itch.io/jogo-da-memoria-guna

https://sesc.digital/colecao/exposicao-encontros-amerindiosMemory