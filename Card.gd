extends TextureButton

class_name Card

var value
#imagens
var face
#parte de tras das cartas
var back

# Called when the node enters the scene tree for the first time.
func _ready():
	#this is a property of TextureButton and 3 is both option 1 and 2 checked 
	#it s possible to see all this properties if the object is add in the scene as a node
	set_h_size_flags(3)
	set_v_size_flags(3)
	set_expand(true)
	set_stretch_mode(TextureButton.STRETCH_KEEP_ASPECT_CENTERED)
	
func _init(var v):
	value = v
	#concatenation of string plus var names since that is how the cards are named in the assets folder
	#and using the str function to transform a number to a string (since the name of the file is a string)	
	face = load("res://assets/cards/card"+str(value)+".png")
	back = GameManager.cardBack
	set_normal_texture(back)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _pressed():
	GameManager.chooseCard(self)
	
func flip():
	if get_normal_texture() == back:
		set_normal_texture(face)	
	else:
		set_normal_texture(back)
